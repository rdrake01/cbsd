/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.airport.entity;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Rustam Drake
 *
 * A sample airports list for testing purposes.
 */
public class AirportsSample {

    List<Airport> list = new ArrayList<>();

    public List<Airport> getSampleList() {
        list.add(new Airport("JFK", "John F Kennedy International"));
        list.add(new Airport("LHR", "London Heathrow"));
        list.add(new Airport("CDG", "Charles de Gaulle International"));
        list.add(new Airport("AMS", "Amsterdam Schiphol Airport"));

        return list;
    }
}
