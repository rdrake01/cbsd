/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.airport.sessionbean;

import com.account.sessionbean.AbstractFacade;
import com.airport.entity.*;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Rustam Drake
 */
@Stateless
public class AirportFacade extends AbstractFacade<Airport> {

    @PersistenceContext(unitName = "travel-reservation-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    /**
     * checks if the IATA code is unique
     *
     * @param iata
     * @return true if iata is unique in the database
     */
    public boolean uniqueIATA(String iata) {
        Query query = em.createNamedQuery("Airport.IATA");
        query.setParameter("iata", iata);
        return query.getResultList().isEmpty();
    }
    /**
     * 
     * @param iata
     * @return the name of the airport from a given three leter IATA code
     */
    public String airportNameFromIATA(String iata){
        Query query = em.createNamedQuery("Airport.IATA");
        query.setParameter("iata", iata);
        return query.getSingleResult().toString();
    }

    /**
     * Creates a Sample database of airports from the AirportsSample.java
     */
    public void crateDataBase() {
        AirportsSample sample = new AirportsSample();
        List<Airport> airportsList = sample.getSampleList();
        airportsList.stream().forEach((a) -> {
            if (uniqueIATA(a.getIataCode())) {
                em.persist(a);
            }
        });
    }

    //TODO: not complete
    public List<Airport> getAllAirports() {
//        List<Airport> airportList = this.findAll();
        return this.findAll();

    }

    public AirportFacade() {
        super(Airport.class);
    }

}
