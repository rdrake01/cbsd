/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flights.sessionbean;

import com.account.sessionbean.AbstractFacade;
import com.flights.entity.Flight;
import com.flights.entity.FlightSample;
import java.util.List;
import javax.ejb.Stateful;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Rustam Drake
 */
@Stateful
public class FlightFacade extends AbstractFacade<Flight> {

    @PersistenceContext(unitName = "travel-reservation-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    /**
     *
     * @param flightNo
     * @return the flight record with the unique Flight Number
     */
    public Flight findFlightByFlighNo(int flightNo) {
        Query query = em.createNamedQuery("Flight.FlightNo");
        query.setParameter("flightNo", flightNo);
        return (Flight) query.getSingleResult();
    }

    public void updateFlightSeats(Flight flight, int businessReserved, int economyReserved) {
        flight.setBusiness(flight.getBusiness()-businessReserved);
        flight.setEconomy(flight.getEconomy()-economyReserved);
        em.merge(flight);
    }

    /**
     *
     * @param from
     * @param to
     * @return list of flight flying 'from' airport to the 'to' airport
     */
    public List<Flight> fromToFlights(String from, String to) {
        Query query = em.createNamedQuery("Flight.FromTo");
        query.setParameter("from", from);
        query.setParameter("to", to);
        return query.getResultList();
    }

    public boolean uniqueFlightNo(int flightNo) {
        Query query = em.createNamedQuery("Flight.FlightNo");
        query.setParameter("flightNo", flightNo);
        return query.getResultList().isEmpty();
    }

    public Flight getInfoForFlightNo(int flightNo) {
        Query query = em.createNamedQuery("Flight.FlightNo");
        query.setParameter("flightNo", flightNo);
        return (Flight) query.getSingleResult();
    }

    public void crateDataBase() {
        FlightSample sample = new FlightSample();
        List<Flight> flightsList = sample.getSampleFlightList();
        flightsList.stream().forEach((f) -> {
            if (uniqueFlightNo(f.getFlightNumber())) {
                em.persist(f);
            }
        });
    }

    public FlightFacade() {
        super(Flight.class);
    }

}
