/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flights.entity;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Rustam Drake
 */
public class FlightSample {

    List<Flight> list = new ArrayList<>();

    public List<Flight> getSampleFlightList() {
        Flight f1 = createFlight(001, "JFK", "LHR", 12);
        f1 = initDate(f1, 2016, 4, 29, 17, 30);
        
        Flight f2 = createFlight(002, "JFK", "LHR", 12);
        f2 = initDate(f2, 2016, 5, 2, 2, 0);
       
        Flight f3 = createFlight(003, "JFK", "CDG", 15);
        f3 = initDate(f3, 2016, 4, 29, 15, 30);
        
        Flight f4 = createFlight(004, "JFK", "AMS", 16);
        f4 = initDate(f4, 2016, 4, 29, 17, 30);
        
        list.add(f1);
        list.add(f2);
        list.add(f3);
        list.add(f4);
        return list;
    }

    private Flight createFlight(int flightNumber, String from, String to, int flightDuration) {
        Flight flight = new Flight();
        flight.setFlightNumber(flightNumber);
        flight.setFrom_Airport(from);
        flight.setTo_Airport(to);
        flight.setFlight_Duration(flightDuration);
        return flight;
    }

    private Flight initDate(Flight flight, int year, int month, int day, int hour, int min) {
        Calendar myCal = Calendar.getInstance();
        myCal.set(Calendar.YEAR, year);
        myCal.set(Calendar.MONTH, month);
        myCal.set(Calendar.DAY_OF_MONTH, day);
        myCal.set(Calendar.HOUR_OF_DAY, hour);
        myCal.set(Calendar.MINUTE, min);
        flight.setFlight_Date(myCal.getTime());
        flight.setFlight_Time(myCal.getTime());

        return flight;
    }

}
