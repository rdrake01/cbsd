/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flights.entity;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Contains flights information: departure, arrival, date of departure, flight duration
 *
 * @author Rustam Drake
 */

@NamedQueries({
    @NamedQuery(name = "Flight.FromTo", 
            query = "SELECT f FROM Flight f WHERE f.from_Airport = :from "
                    + "AND f.to_Airport = :to"),
    
    @NamedQuery(name = "Flight.FlightNo",
            query = "SELECT f FROM Flight f WHERE f.flightNumber = :flightNo")
        
})
@Entity
public class Flight implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private int flightNumber;
    private String from_Airport;
    private String to_Airport;
    @Temporal(TemporalType.DATE)
    private Date flight_Date;
    @Temporal(TemporalType.TIME)
    private Date flight_Time;
    private int flight_Duration;
    private int business;
    private int economy;
    
    Flight(){
        this.economy = 90;
        this.business = 10;
    }
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFrom_Airport() {
        return from_Airport;
    }

    public void setFrom_Airport(String from_Airport) {
        this.from_Airport = from_Airport;
    }

    public String getTo_Airport() {
        return to_Airport;
    }

    public void setTo_Airport(String to_Airport) {
        this.to_Airport = to_Airport;
    }

    public Date getFlight_Date() {
        return flight_Date;
    }

    public void setFlight_Date(Date flight_Date) {
        this.flight_Date = flight_Date;
    }

    public Date getFlight_Time() {
        return flight_Time;
    }

    public void setFlight_Time(Date flight_Time) {
        this.flight_Time = flight_Time;
    }

    public int getFlight_Duration() {
        return flight_Duration;
    }

    public void setFlight_Duration(int flight_Duration) {
        this.flight_Duration = flight_Duration;
    }

    public int getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(int flightNumber) {
        this.flightNumber = flightNumber;
    }

    public int getBusiness() {
        return business;
    }

    public void setBusiness(int business) {
        this.business = business;
    }

    public int getEconomy() {
        return economy;
    }

    public void setEconomy(int economy) {
        this.economy = economy;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Flight)) {
            return false;
        }
        Flight other = (Flight) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.flights.entity.Flight[ id=" + id + " ]";
    }
    
}
