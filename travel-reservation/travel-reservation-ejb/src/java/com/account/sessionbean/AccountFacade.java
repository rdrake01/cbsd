/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.account.sessionbean;

import com.account.entity.Account;
import com.flights.entity.Flight;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.Query;

/**
 *
 * @author Rustam Drake
 */
@Stateful
public class AccountFacade extends AbstractFacade<Account> {
    
    private static final Logger logger=Logger.getLogger(AccountFacade.class.getName());

    @PersistenceContext(unitName = "travel-reservation-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public AccountFacade() {
        super(Account.class);
    }
    
    /**
     * finds the account information by using an Email
     * @param email
     * @return a single Account record from database
     */
    public Account findAccountByEmail(String email){
        Query query = em.createNamedQuery("Account.getAccountFromEmail");
        query.setParameter("email", email);
        return  (Account)query.getSingleResult();
    }
    
    public boolean uniqueUserValidator(String email){
        Query query = em.createNamedQuery("Account.findAll");
        List<Account> accountLoginInfo = query.getResultList();
        
        return accountLoginInfo.stream().noneMatch((a) -> (a.getEmail().equals(email)));
    }

    public boolean loginValidator(String email, String password) {
        Query query=em.createNamedQuery("Account.findAll");
        List<Account> accountLoginInfo = query.getResultList();
        for(Account a :accountLoginInfo){
            logger.log(Level.SEVERE, email);
            logger.log(Level.SEVERE, password);
            if (a.getEmail().equals(email) && a.getPassward().equals(password)==true){
                return true;
            }
        }
        return false;
        /*
        TypedQuery<Account> query = em.createQuery("SELECT a FROM Account a ", Account.class);
        List<Account> accountLoginInfo = query.getResultList();
        for (Account a : accountLoginInfo) {
        logger.log(Level.FINE, email);
        logger.log(Level.FINE, password);
        if (a.getEmail().equals(email) && a.getPassward().equals(password)) {
        return true;
        }
        } 
         */        
    }

}
