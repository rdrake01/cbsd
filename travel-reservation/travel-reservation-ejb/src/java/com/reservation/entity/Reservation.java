/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.reservation.entity;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

/**
 *
 * @author Rustam Drake
 */
@NamedQueries({
    @NamedQuery(name = "Reservation.FindAll",
            query = "SELECT r FROM Reservation r "),
    @NamedQuery(name = "Reservation.findReservationByEmail",
            query = "SELECT r FROM Reservation r WHERE r.email=:email")
})
@Entity
public class Reservation implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String email;
    private Integer flightNumber;
    private Integer businessReserved;
    private Integer economyReserved;
    private boolean payed;
    

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(Integer flightNumber) {
        this.flightNumber = flightNumber;
    }

    public Integer getBusinessReserved() {
        return businessReserved;
    }

    public void setBusinessReserved(Integer businessReserved) {
        this.businessReserved = businessReserved;
    }

    public Integer getEconomyReserved() {
        return economyReserved;
    }

    public void setEconomyReserved(Integer economyReserved) {
        this.economyReserved = economyReserved;
    }

    public boolean isPayed() {
        return payed;
    }

    public void setPayed(boolean payed) {
        this.payed = payed;
    }
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Reservation)) {
            return false;
        }
        Reservation other = (Reservation) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.reservation.entity.Reservation[ id=" + id + " ]";
    }
    
}
