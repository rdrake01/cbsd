/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.reservation.sessionbean;

import com.reservation.entity.Reservation;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Rustam Drake
 */
@Stateless
public class ReservationFacade extends AbstractFacade<Reservation> {

    @PersistenceContext(unitName = "travel-reservation-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public void updatePayed(Reservation reservation) {
        reservation.setPayed(true);
        em.merge(reservation);
    }

    public void removeReservation(Reservation reservation) {
        this.remove(reservation);
    }

    public List<Reservation> findAllReservationByEmail(String email) {
        Query query = em.createNamedQuery("Reservation.findReservationByEmail");
        query.setParameter("email", email);
        return query.getResultList();
    }

    public ReservationFacade() {
        super(Reservation.class);
    }

}
