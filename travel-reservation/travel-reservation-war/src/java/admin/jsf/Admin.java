/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package admin.jsf;

import com.airport.sessionbean.AirportFacade;
import com.airport.entity.Airport;
import com.airport.entity.AirportsSample;
import com.flights.sessionbean.FlightFacade;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;

/**
 *
 * @author Rustam Drake
 */
@Named(value = "admin")
@SessionScoped
public class Admin implements Serializable {

    @EJB
    AirportFacade airportFacade;
    @EJB
    FlightFacade flightFacade;

    Airport airport;
    String iata;
    String airportName;

    /**
     * Creates a new instance of Admin
     */
    public Admin() {
        this.airport = new Airport();
    }

    public String getIata() {
        return iata;
    }

    public void setIata(String iata) {
        this.iata = iata;
    }

    public String getAirportName() {
        return airportName;
    }

    public void setAirportName(String airportName) {
        this.airportName = airportName;
    }

    public void addAirport() {
        System.out.println("----inside addAirport() method");
        long idNumber = 0;
        airport.setIataCode(iata);
        airport.setAirportName(airportName);
        airport.setId(idNumber++);
        this.airportFacade.create(airport);
    }

    public void buildAirportsSampleDatabase() {
        airportFacade.crateDataBase();
    }

    public void buildFlightsSampleDatabase() {
        flightFacade.crateDataBase();
    }

}
