/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.account.jsf;

import com.account.entity.Account;
import com.account.sessionbean.AccountFacade;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;

/**
 *
 * @author Rustam Drake
 */
@Named(value = "registerController")
@SessionScoped
public class RegisterController implements Serializable {

    @EJB
    private AccountFacade accountFacade;

    static final Logger logger = Logger.getLogger(RegisterController.class.getName());
    private Account account;
    private String firstName;
    private String lastName;
    private String email;
    private String password;
    private Long id;
    private String sixteenDigitCardNumber;
    private String expirationDate;

    public RegisterController() {
        this.account = new Account();
    }
    
    public String getSixteenDigitCardNumber() {
        return sixteenDigitCardNumber;
    }

    public void setSixteenDigitCardNumber(String sixteenDigitCardNumber) {
        this.sixteenDigitCardNumber = sixteenDigitCardNumber;
    }

    public String getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(String expirationDate) {
        this.expirationDate = expirationDate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String createAccount() {
        long idNumber = 0;
        boolean b = accountFacade.uniqueUserValidator(email);
        if (!b) {
            logger.log(Level.SEVERE, "Email is not unique");
            return "error.xhtml";
        } else {
            account.setExpirationDate(expirationDate);
            account.setSixteenDigitCardNumber(sixteenDigitCardNumber);
            account.setNameFirst(firstName);
            account.setNameLast(lastName);
            account.setEmail(email);
            account.setPassward(password);
            account.setId(idNumber++);

            this.accountFacade.create(account);
            return "success.xhtml";
        }
    }

    public String registerNewAccount() {
        return "register/registerNewAccount.xhtml";
    }
}
