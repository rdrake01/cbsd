/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.account.jsf;

import com.account.sessionbean.AccountFacade;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;

/**
 *
 * @author ali
 */
@Named(value = "account")
@RequestScoped
public class AccountManagedBean {

    @EJB
    AccountFacade accontFacade;

    /**
     * Creates a new instance of Account
     */
    public AccountManagedBean() {
    }

    public String getCardNumber(String email) {
       return ""+accontFacade.findAccountByEmail(email).getSixteenDigitCardNumber();
       
    }

    public String getCardExpirationDate(String email) {
        return accontFacade.findAccountByEmail(email).getExpirationDate();
        
    }

    /**
     * @param email
     * @return full name as a String
     */
    public String getAccountNameFull(String email) {
        String name1 = accontFacade.findAccountByEmail(email).getNameFirst();
        String name2 = accontFacade.findAccountByEmail(email).getNameLast();
        return name1 + " " + name2;
    }
}
