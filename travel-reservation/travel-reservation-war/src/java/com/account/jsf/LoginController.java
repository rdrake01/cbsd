/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.account.jsf;

import com.account.sessionbean.AccountFacade;
import java.io.Serializable;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

/**
 *
 * @author Rustam Drake
 */
@Named(value = "loginController")
@SessionScoped
public class LoginController implements Serializable {

    @EJB
    private AccountFacade accountFacede;

    private String email=null;
    private String password=null;

    /**
     * login controller
     * 
     * @return sends you to page depending if your login credentials are correct 
     */
    public String login() {
        boolean loginInfoCheck = accountFacede.loginValidator(email, password);
        if (loginInfoCheck == true) {
            return "login/login";
        } else {
            this.setEmail(null);
            this.setPassword(null);
            return "login/error";
        }
    }
    /**
     * logout controller
     */
    public void logout(){
        this.setEmail(null);
        this.setPassword(null);
    }
    public boolean checkedIfLoggedIn() {
        return email != null;
    }
    
    //bellow are getters & setters
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

   

}
