/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.account.jsf;

import com.reservation.entity.Reservation;
import com.reservation.sessionbean.ReservationFacade;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.Dependent;

/**
 *
 * @author Rustam Drake
 */
@Named(value = "profile")
@Dependent
public class Profile {
    
    @EJB
    ReservationFacade reservationFacade;

    /**
     * Creates a new instance of Profile
     */
    public Profile() {
    }
    
    public String pay(Reservation reservation){
        reservationFacade.updatePayed(reservation);
        return "booking/payment_accepted";
    }
    
    public void cancelFlight(Reservation reservation){
        reservationFacade.removeReservation(reservation);
        //return "booking/payment_accepted";
    }

    public List<Reservation> getAllResermationForUser(String email){        
        return reservationFacade.findAllReservationByEmail(email);
    }
}
