/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flights.jsf;

import com.airport.entity.Airport;
import com.flights.entity.Flight;
import com.airport.sessionbean.AirportFacade;
import com.flights.sessionbean.FlightFacade;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;

/**
 *
 * @author Rustam Drake
 */
@Named(value = "flightsManagedBean")
@SessionScoped
public class FlightsManagedBean implements Serializable {

    /**
     * Creates a new instance of FlightsManagedBean
     */
    public FlightsManagedBean() {
    }

    @EJB
    FlightFacade flightFacade;
    @EJB
    AirportFacade airportFacade;

    private String airportName;
    private String to;
    private String from;
    private String toAirportName;
    private String fromAirportName;
    private String iata;
    private int flightNo;
    private Date flightDate;
    private Date flightTime;
    private int business;
    private int economy;

    public List<Airport> getAllAirports() {
        return airportFacade.findAll();
    }

    /**
     * @return message for the selected airports
     */
    public String getMessageFromAirportsSelection() {
        if ("".equals(from) || from == null || "".equals(to) || to == null) {
            return "";
        } else if (from.equals(to)) {
            return "YOU CAN'T SELECT THE SAME AIRPORT";
        } else {
            return "Flights from " + airportFacade.airportNameFromIATA(from)
                    + " to " + airportFacade.airportNameFromIATA(to) + ":";
        }
    }

    /**
     *
     * @return a String of all flight numbers flying from 'from' airport to 'to'
     * airport
     */
    public String getFlightNoFromToFlightsAsString() {
        String temp = "";
        temp = flightFacade.fromToFlights(from, to).stream().map((f)
                -> f.getFlightNumber() + ", ").reduce(temp, String::concat);
        if ("".equals(temp)) {
            return "";
        } else {
            return temp;
        }
    }

    /**
     *
     * @return
     */
    public List<Flight> getFromToFlights() {
        if (flightFacade.fromToFlights(from, to) == null) {
            return null;
        } else {
            return flightFacade.fromToFlights(from, to);
        }
    }

    /**
     * called when a flight has been selected from the table in the booking home
     *
     * @param flight
     * @return sends you to the booking page
     */
    public String goToBookingPage(Flight flight) {
        this.flightNo = flight.getFlightNumber();
        this.flightDate = flight.getFlight_Date();
        this.flightTime = flight.getFlight_Time();
        this.from = flight.getFrom_Airport();
        this.to = flight.getTo_Airport();
        this.toAirportName = airportFacade.airportNameFromIATA(to);
        this.fromAirportName = airportFacade.airportNameFromIATA(from);
        getNumberOfSeatsAvailable(flightNo);
        return "booking_page";
    }
    /**
     * get the number of seats available for the given flight number
     * @param flightNo 
     */
    private void getNumberOfSeatsAvailable(int flightNo){
        this.business = flightFacade.getInfoForFlightNo(flightNo).getBusiness();
        this.economy = flightFacade.getInfoForFlightNo(flightNo).getEconomy();
    }
    //getters & setters bellow
    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getAirportName() {
        return airportName;
    }

    public void setAirportName(String airportName) {
        this.airportName = airportName;
    }

    public String getIata() {
        return iata;
    }

    public void setIata(String iata) {
        this.iata = iata;
    }

    public int getFlightNo() {
        return flightNo;
    }

    public void setFlightNo(int flightNo) {
        this.flightNo = flightNo;
    }

    public Date getFlightDate() {
        return flightDate;
    }

    public void setFlightDate(Date flightDate) {
        this.flightDate = flightDate;
    }

    public Date getFlightTime() {
        return flightTime;
    }

    public void setFlightTime(Date flightTime) {
        this.flightTime = flightTime;
    }

    public String getToAirportName() {
        return toAirportName;
    }

    public void setToAirportName(String toAirportName) {
        this.toAirportName = toAirportName;
    }

    public String getFromAirportName() {
        return fromAirportName;
    }

    public void setFromAirportName(String fromAirportName) {
        this.fromAirportName = fromAirportName;
    }

    public int getBusiness() {
        return business;
    }

    public void setBusiness(int business) {
        this.business = business;
    }

    public int getEconomy() {
        return economy;
    }

    public void setEconomy(int economy) {
        this.economy = economy;
    }

}
