/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flights.jsf;

import com.airport.sessionbean.AirportFacade;
import com.flights.entity.Flight;
import com.flights.sessionbean.FlightFacade;
import com.reservation.entity.Reservation;
import com.reservation.sessionbean.ReservationFacade;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;

/**
 *
 * @author Rustam Drake
 */
@Named(value = "reservation")
@RequestScoped
public class ReservationManagedBean {

    @EJB
    FlightFacade flightFacade;
    @EJB
    AirportFacade airportFacade;
    @EJB
    ReservationFacade reservationFacade;
    private Reservation reservation;
    private Integer businessReserved = 0;
    private Integer economyReserved = 0;
    private Long id;

    /**
     * Creates a new instance of ReservationManagedBean
     */
    public ReservationManagedBean() {
        this.reservation = new Reservation();
    }

    public String pay() {
        reservation.setPayed(true);
        reservationFacade.edit(reservation);
        return "payment_accepted";
    }

    /**
     * used for reserving seats, not paying for it
     *
     * @param flightNo
     * @param email
     * @return
     */
    public String reserve(int flightNo, String email) {
        Flight flight = flightFacade.findFlightByFlighNo(flightNo);
        int availableBusinesSeats = flightFacade.findFlightByFlighNo(flightNo).getBusiness();
        int availableEconomySeats = flightFacade.findFlightByFlighNo(flightNo).getEconomy();
        if (businessReserved >= availableBusinesSeats || economyReserved >= availableEconomySeats) {
            return "too_many_seats_reserved";
        } else {
            System.out.println("---" + businessReserved + "---");
            this.reservation.setBusinessReserved(businessReserved);

            System.out.println("---" + economyReserved + "---");
            this.reservation.setEconomyReserved(economyReserved);

            System.out.println("---" + flightNo + "---");
            this.reservation.setFlightNumber(flightNo);

            System.out.println("---" + email + "---");
            this.reservation.setEmail(email);

            this.reservation.setPayed(false);
            flightFacade.updateFlightSeats(flight, businessReserved, economyReserved);
            reservationFacade.create(this.reservation);
            return "pay";
        }
    }

    //getters and setters bellow
    public Integer getBusinessReserved() {
        return businessReserved;
    }

    public void setBusinessReserved(Integer businessReserved) {
        this.businessReserved = businessReserved;
    }

    public Integer getEconomyReserved() {
        return economyReserved;
    }

    public void setEconomyReserved(Integer economyReserved) {
        this.economyReserved = economyReserved;
    }

}
